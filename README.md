# tty

tty, pty

* [how to emulate a tty with shell
  ](https://www.google.com/search?q=how+to+emulate+tty+with+shell)
* [*How to make output of any shell command unbuffered?*
  ](https://stackoverflow.com/questions/3465619/how-to-make-output-of-any-shell-command-unbuffered)
  (using `stdbuf`, `unbuffer`, `script`) (2021) Stack Overflow
* [*Pretend to be a tty in bash for any command*
  ](https://stackoverflow.com/questions/32910661/pretend-to-be-a-tty-in-bash-for-any-command)
  (using `script` + `printf`, `unbuffer`) (2021) Stack Overflow
* [*How to trick an application into thinking its stdout is a terminal, not a pipe*
  ](https://stackoverflow.com/questions/1401002/how-to-trick-an-application-into-thinking-its-stdout-is-a-terminal-not-a-pipe)
  (using `script` + `printf`, `unbuffer`, C programming language) (2021) Stack Overflow
* [*Emulate PTY and output line-flushed stream*
  ](https://stackoverflow.com/questions/16004183/emulate-pty-and-output-line-flushed-stream)
  (using `socat`) (2013) Stack Overflow
* [*Detect if stdin is a terminal or pipe?*
  ](https://stackoverflow.com/questions/1312922/detect-if-stdin-is-a-terminal-or-pipe)
  (programming in C or Python) (2019) Stack Overflow
